﻿using System;
using System.Collections.Generic;

namespace Project_PRN221_1.Models
{
    public partial class Post
    {
        public Post()
        {
            Comments = new HashSet<Comment>();
        }

        public int PostId { get; set; }
        public int CategoryId { get; set; }
        public int UserId { get; set; }
        public string PostGraph1 { get; set; } = null!;
        public string? PostGraph2 { get; set; }
        public string? PostGraph3 { get; set; }
        public string? PostGraph4 { get; set; }
        public string? PostGraph5 { get; set; }
        public DateTime PostDate { get; set; }
        public string? Image1Path { get; set; }
        public string? Image2Path { get; set; }
        public string? Image3Path { get; set; }
        public string? Image4Path { get; set; }
        public string? Image5Path { get; set; }
        public string PostHeader { get; set; } = null!;

        public int? IsCensor { get; set; }

        public virtual Category Category { get; set; } = null!;
        public virtual User User { get; set; } = null!;
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
