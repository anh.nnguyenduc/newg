using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Admin
{
    public class UserManageModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public UserManageModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }

        public List<User> Users { get; set; }

        [BindProperty]
        public User u { get; set; } 
        public void OnGet()
        {
            Categories = _context.Categories.ToList();
            Users = _context.Users.Include(x => x.Role).ToList(); 

        }
        // delete user
        public IActionResult OnPost(int userId)
        {
			Categories = _context.Categories.ToList();
            
            var userToDelete = _context.Users.FirstOrDefault(u => u.UserId == userId);
            if (userToDelete != null)
            {
                var userPosts = _context.Posts.Where(p => p.UserId == userId).ToList();
                foreach (var post in userPosts)
                {
                    var postComments = _context.Comments.Where(c => c.PostId == post.PostId).ToList();
                    foreach (var comment in postComments)
                    {
                        _context.Comments.Remove(comment);
                    }
                    _context.Posts.Remove(post);
                }

                var userComments = _context.Comments.Where(c => c.UserId == userId).ToList();
                foreach (var comment in userComments)
                {
                    _context.Comments.Remove(comment);
                }
                _context.Users.Remove(userToDelete);
                _context.SaveChanges();
            }
            Users = _context.Users.Include(x => x.Role).ToList();
            return Page();
		}

        // set as censor
        public IActionResult OnPostSetCensor(int userId)
        {
            Categories = _context.Categories.ToList();
            var userToCensor = _context.Users.FirstOrDefault(u => u.UserId == userId);
            if (userToCensor != null)
            {
                userToCensor.RoleId = 3;
                _context.Users.Update(userToCensor);
                _context.SaveChanges();
            }
            Users = _context.Users.Include(x => x.Role).ToList();
            return Page();
        }
        //unset censor
        public IActionResult OnPostUnsetCensor(int userId)
        {
            Categories = _context.Categories.ToList();
            var userToUncensor = _context.Users.FirstOrDefault(u => u.UserId == userId);
            if (userToUncensor != null)
            {
                userToUncensor.RoleId = 1;
                _context.Users.Update(userToUncensor);
                _context.SaveChanges();
            }
            Users = _context.Users.Include(x => x.Role).ToList();
            return Page();
        }

        //deactive user
        public IActionResult OnPostDeactive(int userId)
        {
            Categories = _context.Categories.ToList();
            var userToDeactive = _context.Users.FirstOrDefault(u => u.UserId == userId);
            if (userToDeactive != null)
            {
                userToDeactive.IsActive = 0;
                _context.Users.Update(userToDeactive);
                _context.SaveChanges();
            }
            Users = _context.Users.Include(x => x.Role).ToList();
            return Page();
        }
    }
}
