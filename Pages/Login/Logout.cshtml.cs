using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Login
{
    public class LogoutModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public LogoutModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }
        public IActionResult OnGet()
        {
            Categories = _context.Categories.ToList();
            HttpContext.Session.Remove("Username");
            HttpContext.Session.Remove("UserNameDisp");
			HttpContext.Session.Remove("UserId");
			HttpContext.Session.Remove("Role");
			return RedirectToPage("../Index");
        }
    }
}
