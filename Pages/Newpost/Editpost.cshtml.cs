using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Newpost
{
    public class EditpostModel : PageModel
    {
		private readonly Project_PRN221_1Context _context;
        [BindProperty]
        public Post postDetail { get; set; }

        public EditpostModel(Project_PRN221_1Context context)
		{
			_context = context;
		}

		public List<Category> Categories { get; set; }
		public void OnGet(int id)
        {
			Categories = _context.Categories.ToList();
            postDetail = _context.Posts.Find(id);
        }
        public IActionResult OnPost()
        {
            postDetail.PostDate = DateTime.Now;
            _context.Posts.Update(postDetail);
            _context.SaveChanges();

            return RedirectToPage("../Index");
        }
    }
}
