using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Login
{
    public class ForgotpasswordenterModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public ForgotpasswordenterModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string OTP { get; set; }

        public List<Category> Categories { get; set; }
        public void OnGet(string email)
        {
            Categories = _context.Categories.ToList();  
            Email = email;
        }

        public IActionResult OnPost()
        {
            Categories = _context.Categories.ToList();
            if (OTP  == "1231")
            {
                return Redirect("Resetpassword?email=" + Email);
            }
            else
            {
                ModelState.AddModelError("OTP", "Wrong otp, enter again.");
                return Page();
            }
        }
    }
}
