using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Login
{
    public class ResetpasswordModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public ResetpasswordModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        [BindProperty]
        public string Email { get; set; }

        public List<Category> Categories { get; set; }

        public User u { get; set; }

        [BindProperty]
        public string Password { get; set; }

        [BindProperty]
        public string ConfirmPassword { get; set; }
        public void OnGet(string email)
        {
            Categories = _context.Categories.ToList();
            Email = email;
            u = _context.Users.FirstOrDefault(x => x.Email == Email);
        }

        public IActionResult OnPost()
        {
            Categories = _context.Categories.ToList();
            u = _context.Users.FirstOrDefault(x => x.Email == Email);
            if (u == null)
            {
                return Page();
            }
            else
            {
                if (u.Password == Password)
                {
                    ModelState.AddModelError("password", "New password is same with current password");
                    return Page();
                }
                else if (ConfirmPassword != Password)
                {
                    ModelState.AddModelError("ConfirmPassword", "Confirm password not match new password");
                    return Page();
                }
                else
                {
                    u.Password = Password;
                    _context.Users.Update(u);
                    _context.SaveChanges();
                }
                return RedirectToPage("Login");
            }
           
        }
    }
}
