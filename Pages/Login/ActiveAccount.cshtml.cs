using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Login
{
    public class ActiveAccountModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public ActiveAccountModel(Project_PRN221_1Context context)
        {
            _context = context;
        }
        public List<Category> Categories { get; set; }
        [BindProperty]
        public string Email { get; set; }
        [BindProperty]
        public string OTP { get; set; }

        public User u { get; set; }
        public void OnGet(string email)
        {
            Categories = _context.Categories.ToList();
            Email = email;
        }

        public IActionResult OnPost()
        {
            Categories = _context.Categories.ToList();
            if (OTP == "0411")
            {
                u = _context.Users.FirstOrDefault(x => x.Email == Email);
                u.IsActive = 1;
                _context.Users.Update(u);
                _context.SaveChanges();
                return RedirectToPage("Login");
            }
            else
            {
                ModelState.AddModelError("OTP", "Wrong otp, enter again");
                return Page();
            }
        }
    }
}
