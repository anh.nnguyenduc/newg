using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages
{
    public class ArticleDetailsModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public Post postDetail;

        public User user;

        public List<Comment> comment;

        public List<Category> Categories { get; set; }
        [BindProperty]
        public string postCensorId { get; set; }


		public ArticleDetailsModel(Project_PRN221_1Context context)
        {
            _context = context;
        }
        public void OnGet(string id)
        {
            if (id == null)
            {
                RedirectToPage("Error");
            }
            else
            {
                Categories = _context.Categories.ToList();
                postDetail = _context.Posts.Find(int.Parse(id));
                user = _context.Users.FirstOrDefault(u => u.UserId == postDetail.UserId);
                comment = _context.Comments.Include(c => c.User).Where(c => c.PostId == int.Parse(id)).ToList();
            }
        }

        public IActionResult OnPost()
        {
			var postDetail = _context.Posts.FirstOrDefault(post => post.PostId == int.Parse(postCensorId));

			if (postDetail != null)
			{
				postDetail.IsCensor = 1;

				_context.Posts.Update(postDetail);
				_context.SaveChanges();
			}
			return RedirectToPage("/Index");
        }
    }
}
