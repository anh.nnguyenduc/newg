using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.CommentPosts
{
    public class CommentPostModel : PageModel
    {
		private readonly Project_PRN221_1Context _context;
		public List<Category> Categories { get; set; }
		public Post postDetail;
		public CommentPostModel(Project_PRN221_1Context context)
		{
			_context = context;
		}
		public IActionResult OnPost()
        {
			Categories = _context.Categories.ToList();
			var postId = int.Parse(Request.Form["PostId"]);
			postDetail = _context.Posts.Find(postId);
			var commentContent = Request.Form["CommentContent"];
			var userId = int.Parse(Request.Form["UserId"]);
			
			var commentDate = DateTime.Now;
			Comment c = new Comment();
			c.PostId = postId;
			c.UserId = userId;
			c.CommentContent = commentContent;
			c.CommentDate = commentDate;

			_context.Comments.Add(c);
			_context.SaveChanges();
			return RedirectToPage("/ArticleDetails", new { id = postId });
		}
	}
}
