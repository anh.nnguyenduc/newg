using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Hosting;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Newpost
{
    public class NewpostModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;
        private readonly IWebHostEnvironment _environment;

        public NewpostModel(Project_PRN221_1Context context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public List<Category> Categories { get; set; }

        public string uId { get; set; }

        [BindProperty]
        public Post post { get; set; }

		[BindProperty]
		public IFormFile Image1Path { get; set; }
		[BindProperty]
		public IFormFile Image2Path { get; set; }
		[BindProperty]
		public IFormFile Image3Path { get; set; }
		[BindProperty]
		public IFormFile Image4Path { get; set; }
		[BindProperty]
		public IFormFile Image5Path { get; set; }
		public void OnGet()
        {
            Categories = _context.Categories.ToList();
            uId = HttpContext.Session.GetString("UserId");
        }

        public IActionResult OnPost()
        {
			Categories = _context.Categories.ToList();
			//if (!ModelState.IsValid)
   //         {
   //             return Page();
   //         }
			if (Image1Path != null)
			{
				string wwwRootPath = _environment.WebRootPath;
				string fileName = Path.GetFileNameWithoutExtension(Image1Path.FileName);
				string extension = Path.GetExtension(Image1Path.FileName);
				fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
				string imagePath = Path.Combine(wwwRootPath + "/images/", fileName);

				using (var fileStream = new FileStream(imagePath, FileMode.Create))
				{
					Image1Path.CopyTo(fileStream);
				}

				post.Image1Path = "~/images/" + fileName;
			}
			if (Image2Path != null)
			{
				string wwwRootPath = _environment.WebRootPath;
				string fileName = Path.GetFileNameWithoutExtension(Image2Path.FileName);
				string extension = Path.GetExtension(Image2Path.FileName);
				fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
				string imagePath = Path.Combine(wwwRootPath + "/images/", fileName);

				using (var fileStream = new FileStream(imagePath, FileMode.Create))
				{
					Image2Path.CopyTo(fileStream);
				}

				post.Image2Path = "~/images/" + fileName;
			}
			if (Image3Path != null)
			{
				string wwwRootPath = _environment.WebRootPath;
				string fileName = Path.GetFileNameWithoutExtension(Image3Path.FileName);
				string extension = Path.GetExtension(Image3Path.FileName);
				fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
				string imagePath = Path.Combine(wwwRootPath + "/images/", fileName);

				using (var fileStream = new FileStream(imagePath, FileMode.Create))
				{
					Image3Path.CopyTo(fileStream);
				}

				post.Image3Path = "~/images/" + fileName;
			}
			if (Image4Path != null)
			{
				string wwwRootPath = _environment.WebRootPath;
				string fileName = Path.GetFileNameWithoutExtension(Image4Path.FileName);
				string extension = Path.GetExtension(Image4Path.FileName);
				fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
				string imagePath = Path.Combine(wwwRootPath + "/images/", fileName);

				using (var fileStream = new FileStream(imagePath, FileMode.Create))
				{
					Image4Path.CopyTo(fileStream);
				}

				post.Image4Path = "~/images/" + fileName;
			}
			if (Image5Path != null)
			{
				string wwwRootPath = _environment.WebRootPath;
				string fileName = Path.GetFileNameWithoutExtension(Image5Path.FileName);
				string extension = Path.GetExtension(Image5Path.FileName);
				fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
				string imagePath = Path.Combine(wwwRootPath + "/images/", fileName);

				using (var fileStream = new FileStream(imagePath, FileMode.Create))
				{
					Image5Path.CopyTo(fileStream);
				}

				post.Image5Path = "~/images/" + fileName;
			}
			post.IsCensor = 0;
			_context.Posts.Add(post);
            _context.SaveChanges();
            return RedirectToPage("../Index");
        }
    }
}
