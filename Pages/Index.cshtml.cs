﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages
{
    public class IndexModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public IndexModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }

        public List<Post> PostList { get; set; }

        public Post post { get; set; }

        public string? roleId { get; set; }

        [BindProperty]
        public int postId { get; set; }

		[BindProperty(SupportsGet = true)]
		public string SearchTerm { get; set; }
		public void OnGet(string category)
        {
            Categories = _context.Categories.ToList();
            roleId = HttpContext.Session.GetString("Role");
            if (!string.IsNullOrEmpty(category))
            {
                var categoryId = _context.Categories.Where(c => c.CategoryName == category).Select(c => c.CategoryId).FirstOrDefault();
                if (categoryId != 0)
                {
                    PostList = _context.Posts.Where(post => post.CategoryId == categoryId).ToList();
                }
            }
            else
            {
                PostList = _context.Posts.OrderByDescending(p => p.PostDate).ToList();
            }
        }
        public IActionResult OnPost(string category)
        {
            Categories = _context.Categories.ToList();
            if (!string.IsNullOrEmpty(category))
            {
                var categoryId = _context.Categories.Where(c => c.CategoryName == category).Select(c => c.CategoryId).FirstOrDefault();
                if (categoryId != 0)
                {
					PostList = _context.Posts.Where(post => post.CategoryId == categoryId).ToList();
                }
            }
            else
            {
				PostList = _context.Posts.OrderByDescending(p => p.PostDate).ToList();
			}

            return Page();
        }

        public IActionResult OnPostSearch()
        {
			Categories = _context.Categories.ToList();
			PostList = _context.Posts
				.Where(post =>EF.Functions.Like(post.PostHeader, $"%{SearchTerm}%"))
				.OrderByDescending(p => p.PostDate)
				.ToList();
			return Page();
		}

        public IActionResult OnPostDecensor()
        {
			Categories = _context.Categories.ToList();
            if (postId != 0)
            {
                post = _context.Posts.FirstOrDefault(p => p.PostId == postId);
                if (post != null)
                {
                    post.IsCensor = 0;
                    _context.Posts.Update(post);
                    _context.SaveChanges();
                }
            }
			PostList = _context.Posts.OrderByDescending(p => p.PostDate).ToList();
			return Page();
        }

        public IActionResult OnPostDelete()
        {
			Categories = _context.Categories.ToList();
			if (postId != 0)
			{
				post = _context.Posts.FirstOrDefault(p => p.PostId == postId);
				if (post != null)
				{
					var postComments = _context.Comments.Where(c => c.PostId == postId).ToList();
					foreach (var comment in postComments)
					{
						_context.Comments.Remove(comment);
					}
					_context.Posts.Remove(post);
					_context.SaveChanges();
				}
			}
			PostList = _context.Posts.OrderByDescending(p => p.PostDate).ToList();
			return Page();
        }
    }
}