﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.UserManage
{
    public class ProfileModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public string ConfirmPassword;
        public ProfileModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }
        [BindProperty]
        public User user { get; set; }

        public void OnGet()
        {
            Categories = _context.Categories.ToList();
            int uId = int.Parse(HttpContext.Session.GetString("UserId"));
            if (uId != null)
            {
                user = _context.Users.Include(r => r.Role).FirstOrDefault(u => u.UserId == uId);
            }
        }
        public IActionResult OnPostUpdateProfile()
        {
            Categories = _context.Categories.ToList();
            int uId = int.Parse(HttpContext.Session.GetString("UserId"));
            var userFromDb = _context.Users.AsNoTracking().FirstOrDefault(u => u.UserId == user.UserId);
            if (userFromDb != null)
            {
                if (userFromDb.Email != user.Email)
                {
                    var existingUser = _context.Users.FirstOrDefault(u => u.Email == user.Email);
                    if (existingUser != null)
                    {
                        user = _context.Users.Include(r => r.Role).FirstOrDefault(u => u.UserId == uId);
                        ModelState.AddModelError("user.Email", "Email already exists.");
                        return Page();
                    }
                    else
                    {
                        userFromDb.Email = user.Email;
                        _context.Entry(userFromDb).State = EntityState.Modified;
                        _context.SaveChanges();
                        user = _context.Users.Include(r => r.Role).FirstOrDefault(u => u.UserId == uId);
                    }
                }
                else
                {
                    userFromDb.Name = user.Name;
                    userFromDb.Email = user.Email;
                    _context.Entry(userFromDb).State = EntityState.Modified;
                    _context.SaveChanges();
                    user = _context.Users.Include(r => r.Role).FirstOrDefault(u => u.UserId == uId);
                }
            }
            return Page();
        }

        public IActionResult OnPostUpdatePassword()
        {
            int userId = int.Parse(HttpContext.Session.GetString("UserId"));
            user = _context.Users.Include(r => r.Role).FirstOrDefault(u => u.UserId == userId);
            Categories = _context.Categories.ToList();
            string currentPassword = Request.Form["currentPassword"];
            string newPassword = Request.Form["newPassword"];
            string confirmPassword = Request.Form["confirmPassword"];

            if (VerifyCurrentPassword(currentPassword))
            {
                if (newPassword == confirmPassword)
                {
                    if (newPassword != currentPassword)
                    {
                        int roleId = user.RoleId;
                        user.RoleId = roleId;
                        user.Password = newPassword;
                        _context.SaveChanges();
                        return RedirectToPage();
                    }
                    else
                    {
                        ModelState.AddModelError("newPassword", "New password must be different from the current password.");
                    }
                }
                else
                {
                    ModelState.AddModelError("confirmPassword", "Confirm password does not match the new password.");
                }
            }
            else
            {
                ModelState.AddModelError("currentPassword", "Current password is incorrect.");
            }
            return Page();
        }

        private bool VerifyCurrentPassword(string currentPassword)
        {
            bool isMatch = false;
            if (user.Password != null && user.Password.Equals(currentPassword, StringComparison.OrdinalIgnoreCase))
            {
                isMatch = true;
            }
            return isMatch;
        }
    }
}
