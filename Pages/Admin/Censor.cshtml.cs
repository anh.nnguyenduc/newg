using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Admin
{
    public class CensorModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;
        public CensorModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }

        public List<Post> Posts { get; set; }
        public void OnGet()
        {
            Categories = _context.Categories.ToList();
            Posts = _context.Posts.Include(post => post.User).Where(post => post.IsCensor == 0).OrderByDescending(p => p.PostDate).ToList();
        }
    }
}
