using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Project_PRN221_1.Models;

namespace Project_PRN221_1.Pages.Login
{
    public class LoginModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public LoginModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        public List<Category> Categories { get; set; }

        public string activationLink { get; set; }
        public void OnGet()
        {
            Categories = _context.Categories.ToList();
        }
        public IActionResult OnPost(string username, string password)
        {
            Categories = _context.Categories.ToList();
            if (!ModelState.IsValid)
            {
                return Page();
            }
            else
            {
                var user = _context.Users.FirstOrDefault(u=>u.Username == username);
                if (user != null)
                {
                    if (!user.Password.Equals(password))
                    {
                        ModelState.AddModelError("password", "Password incorrect, enter again.");
                        return Page();
                    }
                    else if (user.IsActive == 0)
                    {
                        activationLink = $"/Login/ActiveAccount?email={user.Email}";
                        ModelState.AddModelError("isActive", "This account is not activated");
                        return Page();
                    }
                    else
                    {
                        HttpContext.Session.SetString("Username", username);
                        HttpContext.Session.SetString("UserId", user.UserId.ToString());
                        HttpContext.Session.SetString("UserNameDisp", user.Name);
                        HttpContext.Session.SetString("Role", user.RoleId.ToString());
                        return RedirectToPage("../Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("username", "Username does not exist, enter again.");
                    return Page();
                }
            }
        }
    }
}
