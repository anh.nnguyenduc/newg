﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Project_PRN221_1.Models;
using System.Net.Mail;
using Humanizer;

namespace Project_PRN221_1.Pages.Login
{
    public class RegistModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public RegistModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        [BindProperty]
        public User u { get; set; }

        public List<Category> Categories { get; set; }
        [BindProperty]
        public string ConfirmPassword { get; set; }
        [TempData]
        public string Message { get; set; }

		public string activeCode { get; set; }
        public void OnGet() {
            Categories = _context.Categories.ToList();
        }
        public IActionResult OnPost()
        {
            //if (!ModelState.IsValid)
            //{
            //    return Page();
            //}
            Categories = _context.Categories.ToList();
            if (!u.Password.Equals(ConfirmPassword))
            {
                ModelState.AddModelError("ConfirmPassword", "Confirm password does not match password.");
                return Page();
            }
            else
            {
				var existingUser = _context.Users.FirstOrDefault(user => user.Username == u.Username);
				if (existingUser != null)
				{
					ModelState.AddModelError("u.Username", "Username already exists.");
					return Page();
				}
				var existingEmail = _context.Users.FirstOrDefault(user => user.Email == u.Email);
				if (existingEmail != null)
				{
					ModelState.AddModelError("u.Email", "Email already exists.");
					return Page();
				}
				u.IsActive = 0;
				_context.Users.Add(u);
                _context.SaveChanges();

				SendActivationEmail(u.Email);
				return Redirect("/Login/ActiveAccount?email=" + u.Email);
            }
        }

		private void SendActivationEmail(string recipient)
		{
			string Subject = "Active account";
			activeCode = "0411";
			string Body = "Your active code is: " + activeCode;
			MailMessage mm = new MailMessage();
			mm.To.Add(recipient);
			mm.Subject = Subject;
			mm.Body = Body;
			mm.IsBodyHtml = false;
			mm.From = new MailAddress("ducanh041103@gmail.com");
			SmtpClient smtp = new SmtpClient("smtp.gmail.com");
			smtp.Port = 587;
			smtp.UseDefaultCredentials = false;
			smtp.EnableSsl = true;
			smtp.Credentials = new System.Net.NetworkCredential("ducanh041103@gmail.com", "fiosowcldzetihon");
			smtp.SendMailAsync(mm);
			ViewData["Message"] = "Một thông báo đã gửi đến " + u.Name;
		}
	}
}
