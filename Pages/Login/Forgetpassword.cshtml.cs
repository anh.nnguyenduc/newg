﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Org.BouncyCastle.Cms;
using Project_PRN221_1.Models;
using System.Net.Mail;

namespace Project_PRN221_1.Pages.Login
{
    public class ForgetpasswordModel : PageModel
    {
        private readonly Project_PRN221_1Context _context;

        public ForgetpasswordModel(Project_PRN221_1Context context)
        {
            _context = context;
        }

        [BindProperty]
        public string Email { get; set; }  

        public List<Category> Categories { get; set; }

        public string code { get; set; }
        public void OnGet()
        {
            Categories = _context.Categories.ToList();
        }

        public IActionResult OnPost()
        {
            sendMailForgetPass(Email);
            return Redirect("/Login/forgotpasswordenter?email=" + Email);
        }

        private void sendMailForgetPass(string email)
        {
            string Subject = "OTP to reset password";
            code = "1231";
            string Body = "Your OTP code to reset password is: " + code;
            MailMessage mm = new MailMessage();
            mm.To.Add(email);
            mm.Subject = Subject;
            mm.Body = Body;
            mm.IsBodyHtml = false;
            mm.From = new MailAddress("ducanh041103@gmail.com");
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("ducanh041103@gmail.com", "fiosowcldzetihon");
            smtp.SendMailAsync(mm);
            ViewData["Message"] = "Một thông báo đã gửi đến " + email;
        }
    }
}
